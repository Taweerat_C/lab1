

//TODO Write class Javadoc
public class Student extends Person {
	private long id;
	//@author Taweesoft
	//TODO Write constructor Javadoc
	/*
	 * Get name as String and id as long
	 * call Super-class constructor(Person) because Student is inherited from Person
	 * set current Student'id equals id from another class who create Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/*
	 * 
	 */
	
	public boolean equals(Object other) {
		if(other.getClass() == Student.class){
			Student obj = (Student)other;
			return obj.id == this.id;
		}
		if(other.getClass() == Person.class){
			return super.equals(other);
		}
		return false;
	}
}
